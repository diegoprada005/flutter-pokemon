// ignore_for_file: file_names

import 'dart:convert';

import 'package:pokemons/models/pokemonInfo.dart';

class SearchsPokemons {
  SearchsPokemons({
    required this.name,
    this.sprites,
    required this.results,
  });

  String name;
  Sprites? sprites;
  List<PokemonsDetails> results;

  factory SearchsPokemons.fromJson(String str) =>
      SearchsPokemons.fromMap(json.decode(str));

  factory SearchsPokemons.fromMap(Map<String, dynamic> json) => SearchsPokemons(
        name: json["name"],
        results: List<PokemonsDetails>.from(
            json["results"].map((x) => PokemonsDetails.fromMap(x))),
        sprites: Sprites.fromJson(json["sprites"]),
      );
}

class Sprites {
  Other? other;

  Sprites({this.other});

  factory Sprites.fromJson(Map<String, dynamic> json) => Sprites(
        other: json["other"] == null ? null : Other.fromJson(json["other"]),
      );
}

class OfficialArtwork {
  String? frontDefault;

  OfficialArtwork({this.frontDefault});

  factory OfficialArtwork.fromJson(Map<String, dynamic> json) =>
      OfficialArtwork(
        frontDefault: json["front_default"],
      );
}

class Other {
  OfficialArtwork? officialArtwork;

  Other({this.officialArtwork});

  factory Other.fromJson(Map<String, dynamic> json) => Other(
        officialArtwork: OfficialArtwork.fromJson(json["official-artwork"]),
      );
}
