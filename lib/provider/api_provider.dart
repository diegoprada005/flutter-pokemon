// ignore_for_file: empty_catches

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:pokemons/models/models.dart';

class ApiProvider with ChangeNotifier {
  bool _isActive = false;
  bool get isActive => _isActive;
  set isActive(bool theme) {
    _isActive = theme;
    notifyListeners();
  }

  int _buttonNumber = 0;
  int get buttonNumber => _buttonNumber;
  set buttonNumber(int i) {
    _buttonNumber = i;
    notifyListeners();
  }

  //-------------------------------------------------------
  bool isLoading = true;
  int _limit = 100;
  int _set = 0;
  final String _key = '&offset=';
  final String _baseUrl = 'https://pokeapi.co/api/v2/pokemon?limit=';

  List<Result> pokemonsList = [];
  //Lista del primer endpoint
  List<PokemonsDetails> pokemondetails = [];
  //Lista donde guarda todo los datos de los pokemones
  List<PokemonsDetails> copiaPokemon = [];
  //copia de pokemondetails
  String effects = '';
  String information = '';

  ApiProvider() {
    getPokemons();
  }

  Future getPokemons() async {
    try {
      isLoading = true;
      notifyListeners();
      final resp = await http.get(Uri.parse('$_baseUrl$_limit$_key$_set'));
      if (resp.statusCode == 200) {
        final pokemon = Pokemons.fromJson(resp.body);

        pokemonsList = pokemon.results;

        for (var i in pokemon.results) {
          await getPokemonsId(url: i.url);
        }
        isLoading = false;
        notifyListeners();
      }
    } catch (e) {}
  }

  Future<dynamic> getPokemonsId({required String url}) async {
    try {
      isLoading = true;
      notifyListeners();
      final resp = await http.get(Uri.parse(url));

      if (resp.statusCode == 200) {
        final pokemon = PokemonsDetails.fromJson(resp.body);

        pokemondetails = [...pokemondetails, pokemon];
        copiaPokemon = [...pokemondetails, pokemon];

        isLoading = false;
        Future.delayed(const Duration(seconds: 3));
        notifyListeners();
      }
    } catch (e) {}
  }

  getMorePokemons(context) async {
    try {
      _limit += 100;
      _set += 100;

      final resp = await http.get(Uri.parse('$_baseUrl$_limit$_key$_set'));
      if (resp.statusCode == 200) {
        final pokemon = Pokemons.fromJson(resp.body);
        for (var i in pokemon.results) {
          await getPokemonsId(url: i.url);
        }
      }

      notifyListeners();
    } catch (e) {
      final snackBar = SnackBar(
        content: Text('Hubo un error al conectar al backend $e'),
        action: SnackBarAction(
          label: 'Aceptar',
          onPressed: () {},
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  searchPokemon(String query) {
    if (query.isNotEmpty) {
      copiaPokemon = copiaPokemon.where((i) => i.name.contains(query)).toList();
    } else {
      copiaPokemon = pokemondetails;
    }
    notifyListeners();
  }

  effectPokemons({required int id}) async {
    try {
      final resp =
          await http.get(Uri.parse('https://pokeapi.co/api/v2/ability/$id/'));

      if (resp.statusCode == 200) {
        final efect = jsonDecode(resp.body);
        effects = efect["flavor_text_entries"][13]["flavor_text"];
        notifyListeners();
      }
    } catch (e) {}
  }

  getInformationPokemons({required int id}) async {
    try {
      final resp = await http
          .get(Uri.parse('https://pokeapi.co/api/v2/pokemon-species/$id/'));
      if (resp.statusCode == 200) {
        final informations = jsonDecode(resp.body);

        information = informations["flavor_text_entries"][42]["flavor_text"];

        notifyListeners();
      }
    } catch (e) {}
  }
}
