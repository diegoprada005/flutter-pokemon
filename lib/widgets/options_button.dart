import 'package:flutter/material.dart';
import 'package:pokemons/widgets/constant.dart';
import 'package:provider/provider.dart';

import '../provider/provider.dart';

class About extends StatelessWidget {
  final String effect;
  final String type;
  final String description;

  const About({
    Key? key,
    required this.type,
    required this.effect,
    required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pokemonProvier = Provider.of<ApiProvider>(context);
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 20),
          _Text(text: effect, color: Colors.grey),
          const SizedBox(height: 30),
          Container(
            color:
                getColor(pokemonProvier.pokemondetails[0].types[0].type.name),
            height: 35,
            width: 70,
            child: Center(
              child: Text(
                type.toUpperCase(),
                style: const TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          const SizedBox(height: 30),
          const _Text(text: 'Pokedex Data.', color: Colors.deepOrange),
          const SizedBox(height: 30),
          _Text(text: description, color: Colors.grey)
        ],
      ),
    );
  }
}

class Stats extends StatelessWidget {
  final double hp;
  final double attack;
  final double defense;
  final double sepAtk;
  final double spDf;
  final double speed;
  const Stats(
      {Key? key,
      required this.hp,
      required this.attack,
      required this.defense,
      required this.sepAtk,
      required this.spDf,
      required this.speed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const _Text(text: 'Base Status', color: Colors.deepOrange),
            const SizedBox(height: 10),
            _Statistics(
                statistics: 'HP:          ${hp.toInt()} ', power: hp / 100),
            _Statistics(
                statistics: 'Attack:     ${attack.toInt()} ',
                power: attack / 100),
            _Statistics(
                statistics: 'Defense:  ${defense.toInt()} ',
                power: defense / 100),
            _Statistics(
                statistics: 'Sp. Atk:   ${sepAtk.toInt()}  ',
                power: sepAtk / 100),
            _Statistics(
                statistics: 'Sp Def:    ${spDf.toInt()}  ', power: spDf / 100),
            _Statistics(
                statistics: 'Speed:    ${speed.toInt()}   ',
                power: speed / 100),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.32,
              width: MediaQuery.of(context).size.height,
              child: Stack(
                children: const [
                  Background(top: 50, left: 0, width: 80, roate: 70),
                  Background(top: 160, left: 75, width: 100, roate: 70),
                  Positioned(
                      top: -40,
                      right: -40,
                      child: Background(
                          top: 160, left: 200, width: 230, roate: 70))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _Text extends StatelessWidget {
  final String text;
  final Color color;
  const _Text({Key? key, required this.text, required this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(fontSize: 20, color: color));
  }
}

class _Statistics extends StatelessWidget {
  final double power;
  final String statistics;
  const _Statistics({Key? key, required this.statistics, required this.power})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _Text(text: statistics, color: Colors.grey),

        const SizedBox(width: 10),
        Container(
          decoration: BoxDecoration(
              border: Border.all(width: 1, color: Colors.grey[400]!),
              borderRadius: BorderRadius.circular(20)),
          height: 9,
          width: MediaQuery.of(context).size.width * 0.4,
          child: LinearProgressIndicator(
            color: power < 0.5 ? Colors.white : Colors.deepOrange[300],
            value: power,
            backgroundColor: Colors.grey[400],
          ),
        )

        //
      ],
    );
  }
}

class Evolutions extends StatelessWidget {
  final int id;
  final String img;
  const Evolutions({Key? key, required this.img, required this.id})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 350,
          height: 40,
          child: Text('Evolution:',
              style: TextStyle(color: Colors.deepOrange[600], fontSize: 17)),
        ),
        Row(
          children: [
            Container(margin: const EdgeInsets.only(left: 30)),
            ImgPokemonsEvolutions(
                img: 'https://cdn.traction.one/pokedex/pokemon/$id.png'),
            const SizedBox(width: 10),
            Column(
              children: [
                Text('Lvl 16',
                    style: TextStyle(
                        color: Colors.deepOrange[300],
                        fontWeight: FontWeight.bold)),
                Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(20)),
                    height: 5,
                    width: 80),
              ],
            ),
            const SizedBox(width: 10),
            ImgPokemonsEvolutions(
                img: 'https://cdn.traction.one/pokedex/pokemon/${id + 1}.png'),
          ],
        ),
        const SizedBox(height: 30),
        Row(
          children: [
            Container(margin: const EdgeInsets.only(top: 20, left: 30)),
            ImgPokemonsEvolutions(
                img: 'https://cdn.traction.one/pokedex/pokemon/${id + 1}.png'),
            const SizedBox(width: 10),
            Column(
              children: [
                Text('Lvl 32',
                    style: TextStyle(
                        color: Colors.deepOrange[300],
                        fontWeight: FontWeight.bold)),
                Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(20)),
                    height: 5,
                    width: 80),
              ],
            ),
            const SizedBox(width: 10),
            ImgPokemonsEvolutions(
                img: 'https://cdn.traction.one/pokedex/pokemon/${id + 2}.png'),
          ],
        ),
      ],
    );
  }
}

class ImgPokemonsEvolutions extends StatelessWidget {
  final String img;
  const ImgPokemonsEvolutions({
    Key? key,
    required this.img,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 130,
      child: Image.network(img),
    );
  }
}
