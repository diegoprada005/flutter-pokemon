import 'package:flutter/material.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.35),
            const Text('Cargando...'),
            Image.asset(
              'assets/pokemon.gif',
              height: 400.0,
              width: 400.0,
            ),
          ],
        ),
      ),
    );
  }
}
