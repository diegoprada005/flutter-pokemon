import 'package:flutter/material.dart';

import 'package:pokemons/models/pokemonInfo.dart';
import 'package:pokemons/screen/loading_screen.dart';

import 'package:pokemons/widgets/constant.dart';

import 'package:provider/provider.dart';
import 'package:pokemons/provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ApiProvider>(context);

    return provider.isLoading
        ? const LoadingScreen()
        : Scaffold(
            body: Column(children: [
            Stack(children: const [
              Positioned(
                  left: 39,
                  top: -20,
                  child: Background(left: 200, top: 0, width: 210, roate: 0)),
              _Header()
            ])
          ]));
  }
}

class _Header extends StatelessWidget {
  const _Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: const [_Title(), _SearchPokemon()]);
  }
}

class _Title extends StatelessWidget {
  const _Title({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ApiProvider>(context);

    return Container(
        margin: const EdgeInsets.symmetric(vertical: 80),
        padding: const EdgeInsets.only(right: 190),
        child: Text('Pokédex',
            style: TextStyle(
                color: theme.isActive ? Colors.white : Colors.red,
                fontSize: 40,
                fontWeight: FontWeight.bold)));
  }
}

class _SearchPokemon extends StatelessWidget {
  const _SearchPokemon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ApiProvider>(context);

    bool switchs = provider.isActive;
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.75,
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Container(
              margin: const EdgeInsets.only(left: 300),
              child: Text(switchs ? 'Modo Dark' : ' Modo Light',
                  style: const TextStyle(fontWeight: FontWeight.bold))),
          Row(children: [
            _TextFieldInput(provider: provider),
            Expanded(
                child: SwitchListTile.adaptive(
                    value: provider.isActive,
                    onChanged: (value) {
                      provider.isActive = value;
                    }))
          ]),
          const Expanded(child: _Body())
        ]));
  }
}

class _TextFieldInput extends StatelessWidget {
  const _TextFieldInput({Key? key, required this.provider}) : super(key: key);
  final ApiProvider provider;
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 40,
        margin: const EdgeInsets.only(top: 10, left: 10),
        width: 300,
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.grey[350]),
            child: TextField(
              decoration: InputDecoration(
                  focusedBorder: InputBorder.none,
                  prefixIcon: const Icon(Icons.search),
                  hintText: 'Buscar pokemon',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: const BorderSide(color: Colors.black))),
              onChanged: provider.searchPokemon,
            )));
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pokemonProvier = Provider.of<ApiProvider>(context);

    return SizedBox(
        width: double.infinity,
        child: ListView.builder(
            itemCount: pokemonProvier.copiaPokemon.length,
            itemBuilder: (BuildContext cpikaontext, index) {
              if (index == pokemonProvier.pokemondetails.length - 50) {
                pokemonProvier.getMorePokemons(context);
              }
              return Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: GestureDetector(
                      onTap: () => Navigator.pushNamed(context, 'description',
                          arguments: pokemonProvier.copiaPokemon[index]),
                      child: _TargetPokemon(
                          pokemonsDetails: pokemonProvier.copiaPokemon[index],
                          index: index)));
            }));
  }
}

class _TargetPokemon extends StatelessWidget {
  final int index;
  final PokemonsDetails pokemonsDetails;
  const _TargetPokemon(
      {Key? key, required this.pokemonsDetails, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<ApiProvider>(context);
    return Stack(children: [
      Card(
          color: getColor(pokemonsDetails.types[0].type.name),
          margin: const EdgeInsets.symmetric(horizontal: 20),
          clipBehavior: Clip.antiAlias,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15))),
          child: Stack(children: [
            Stack(children: [
              Row(children: [
                SizedBox(
                    width: 110,
                    height: MediaQuery.of(context).size.height * 0.13,
                    child: Image.network(provider.copiaPokemon[index].sprites!
                        .other!.officialArtwork!.frontDefault!))
              ]),
              Container(
                margin: const EdgeInsets.only(left: 120, top: 10),
                child: Text(pokemonsDetails.name,
                    style: const TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)),
              ),
              Positioned(
                  right: -5,
                  child: Container(
                      margin: EdgeInsets.only(
                          left: index > 1000 ? 190 : 225, top: 60),
                      child: Opacity(
                          opacity: 0.6,
                          child: Text(
                              '${provider.copiaPokemon[index].id}'
                                  .padLeft(2, '00'),
                              style: const TextStyle(
                                  fontSize: 70,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white)))))
            ])
          ]))
    ]);
  }
}
