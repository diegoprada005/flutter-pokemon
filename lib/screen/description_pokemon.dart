import 'package:flutter/material.dart';
import 'package:pokemons/models/pokemonInfo.dart';
import 'package:pokemons/provider/provider.dart';
import 'package:pokemons/screen/loading_screen.dart';

import 'package:pokemons/widgets/constant.dart';
import 'package:pokemons/widgets/options_button.dart';
import 'package:provider/provider.dart';

class DescriptionScreen extends StatelessWidget {
  const DescriptionScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PokemonsDetails pokemonsDetails =
        ModalRoute.of(context)!.settings.arguments as PokemonsDetails;
    final pokemonsProvider = Provider.of<ApiProvider>(context);

    pokemonsProvider.effectPokemons(id: pokemonsDetails.id);
    pokemonsProvider.getInformationPokemons(id: pokemonsDetails.id);

    //pokemonsProvider.getEvolutionsPokemons(id: pokemonsDetails.id);

    final String img =
        pokemonsDetails.sprites!.other!.officialArtwork!.frontDefault!;

    final double hp = pokemonsDetails.stats[0].baseStat.toDouble();
    final double attack = pokemonsDetails.stats[1].baseStat.toDouble();
    final double defense = pokemonsDetails.stats[2].baseStat.toDouble();
    final double sepAtk = pokemonsDetails.stats[3].baseStat.toDouble();
    final double spDf = pokemonsDetails.stats[4].baseStat.toDouble();
    final double speed = pokemonsDetails.stats[5].baseStat.toDouble();

    return pokemonsProvider.isLoading
        ? const LoadingScreen()
        : Scaffold(
            body: Column(children: [
            _Header(
                name: pokemonsDetails.name,
                index: pokemonsDetails.id,
                img: pokemonsDetails
                    .sprites!.other!.officialArtwork!.frontDefault!,
                color: pokemonsDetails.types[0].type.name),
            const _Body(),
            if (pokemonsProvider.buttonNumber == 0)
              About(
                  type: pokemonsDetails.types[0].type.name,
                  effect: pokemonsProvider.effects,
                  description: pokemonsProvider.information)
            else if (pokemonsProvider.buttonNumber == 1)
              Stats(
                  hp: hp,
                  attack: attack,
                  defense: defense,
                  sepAtk: sepAtk,
                  spDf: spDf,
                  speed: speed)
            else if (pokemonsProvider.buttonNumber == 2)
              Evolutions(img: img, id: pokemonsDetails.id)
          ]));
  }
}

class _Header extends StatelessWidget {
  final int index;
  final String img;
  final String color;
  final String name;
  const _Header(
      {Key? key,
      required this.img,
      required this.color,
      required this.index,
      required this.name})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: getColor(color),
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(180),
                bottomRight: Radius.circular(180))),
        height: MediaQuery.of(context).size.height * 0.4,
        width: double.infinity,
        child: Stack(children: [
          Container(
              margin: const EdgeInsets.only(top: 70, left: 20),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 100),
                    child: Text('#${'$index'.padLeft(2, '00')}',
                        style:
                            const TextStyle(fontSize: 20, color: Colors.white)),
                  ),
                  Container(
                      margin: const EdgeInsets.only(left: 10),
                      child: Text(name.toUpperCase(),
                          style: const TextStyle(
                              fontSize: 25,
                              color: Colors.white,
                              fontWeight: FontWeight.bold)))
                ],
              )),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: _Img(index: index),
          ),
          Positioned(
              top: 30,
              child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(Icons.arrow_back, color: Colors.white))),
          Container(
              margin: const EdgeInsets.only(top: 320), child: const _Dots())
        ]));
  }
}

class _Img extends StatelessWidget {
  const _Img({
    Key? key,
    required this.index,
  }) : super(key: key);

  final int index;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            margin: const EdgeInsets.only(top: 41),
            padding: const EdgeInsets.all(90.0),
            child: Image.network(
                'https://cdn.traction.one/pokedex/pokemon/$index.png')),
        Container(
            margin: const EdgeInsets.only(top: 42),
            padding: const EdgeInsets.all(90.0),
            child: Image.network(
                'https://cdn.traction.one/pokedex/pokemon/${index + 1}.png')),
        Container(
            margin: const EdgeInsets.only(top: 42),
            padding: const EdgeInsets.all(90.0),
            child: Image.network(
                'https://cdn.traction.one/pokedex/pokemon/${index + 2}.png')),
      ],
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 80,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: const [
          _ElevatedButton(title: 'About', index: 0),
          _ElevatedButton(title: 'Status', index: 1),
          _ElevatedButton(title: 'Evolutions', index: 2)
        ]));
  }
}

class _ElevatedButton extends StatelessWidget {
  final int index;
  final String title;
  const _ElevatedButton({Key? key, required this.title, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final button = Provider.of<ApiProvider>(context);
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: ElevatedButton(
            style: ElevatedButton.styleFrom(primary: Colors.white),
            onPressed: () {
              button.buttonNumber = index;
            },
            child: Text(title,
                style: TextStyle(
                    color: button.buttonNumber == index
                        ? Colors.deepOrange
                        : Colors.grey[400]))));
  }
}

class _Dots extends StatelessWidget {
  const _Dots({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 70,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          _Dot(),
          SizedBox(width: 5),
          _Dot(),
          SizedBox(width: 5),
          _Dot()
        ],
      ),
    );
  }
}

class _Dot extends StatelessWidget {
  const _Dot({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 12,
        width: 12,
        margin: const EdgeInsets.symmetric(horizontal: 5),
        decoration: const BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
        ));
  }
}
