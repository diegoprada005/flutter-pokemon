import 'package:flutter/material.dart';

import 'package:pokemons/provider/provider.dart';

import 'package:pokemons/screen/screen.dart';
import 'package:provider/provider.dart';

void main() async {
  runApp(const AppState());
}

class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
      ChangeNotifierProvider(create: (_) => ApiProvider()),
    ], child: const MyApp());
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<ApiProvider>(context);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const Scaffold(
          body: Center(
        child: Text('Hello World'),
      )),
      initialRoute: 'start',
      routes: {
        'start': (_) => const HomeScreen(),
        'description': (_) => const DescriptionScreen(),
        'loading': (_) => const LoadingScreen()
      },
      theme: theme.isActive ? ThemeData.dark() : ThemeData.light(),
    );
  }
}
